all: build

build: format test
	elm make src/Main.elm --output tangle-ui.js

format:
	elm-format --yes src/

test:
	elm-test

setup:
	sudo apt install nodejs npm
	sudo npm install --unsafe-perm=true --global elm
	sudo npm install --unsafe-perm=true --global elm-test
	sudo npm install --unsafe-perm=true --global elm-format
