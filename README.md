UI for the game Tangle.  More info at https://gitlab.com/andybalaam/tangle

## Prerequisites

Install [Elm](http://elm-lang.org/), elm-test and elm-format.

On Ubuntu, try:

```bash
make setup
```

## Build & Run

```bash
elm reactor
```

Then browse to http://localhost:8000 .

## License

Copyright 2019 Andy Balaam.

Free Software released under the AGPLv3.  See [LICENSE](LICENSE) for details.
