module PlayUpdate exposing (update)

import Msg exposing (Msg)
import PlayModel exposing (PlayModel)
import PlayMsg exposing (PlayMsg(..))
import PlayUpdate01EnterName
import PlayUpdate02Join
import PlayUpdate03AddBot
import PlayUpdate04PollForStartingIn
import PlayUpdate05PollForRunning


update : PlayMsg -> PlayModel -> ( PlayModel, Cmd Msg )
update playMsg playModel =
    case playMsg of
        NameKeyPressed name ->
            PlayUpdate01EnterName.nameKeyPressed playModel name

        SendJoinRequest name ->
            PlayUpdate01EnterName.sendJoinRequest playModel name

        CancelJoinRequest ->
            PlayUpdate02Join.cancelJoinRequest playModel

        ReceivedJoinSucceeded response ->
            PlayUpdate02Join.receivedJoinSucceeded playModel response

        ReceivedJoinFailed errMsg ->
            PlayUpdate02Join.receivedJoinFailed playModel errMsg

        SendAddBotRequest unstartedGameState ->
            PlayUpdate03AddBot.sendAddBotRequest unstartedGameState playModel

        ReceivedAddBotSucceeded unstartedGameState response ->
            PlayUpdate03AddBot.receivedAddBotSucceeded
                unstartedGameState
                playModel
                response

        ReceivedAddBotFailed errMsg unstartedGameState ->
            PlayUpdate03AddBot.receivedAddBotFailed
                playModel
                errMsg
                unstartedGameState

        ReceivedAddAnotherBotSucceeded startedGameState response ->
            PlayUpdate03AddBot.receivedAddAnotherBotSucceeded
                startedGameState
                playModel
                response

        ReceivedAddAnotherBotFailed errMsg startedGameState ->
            PlayUpdate03AddBot.receivedAddAnotherBotFailed
                playModel
                errMsg
                startedGameState

        CancelPollingForStartingIn ->
            PlayUpdate04PollForStartingIn.cancelPollingForStartingIn playModel

        SendPollForStartingIn unstartedGameState ->
            PlayUpdate04PollForStartingIn.sendPollForStartingIn
                playModel
                unstartedGameState

        ReceivedPollForStartingIn unstartedGameState viewResponse ->
            PlayUpdate04PollForStartingIn.receivedPollForStartingIn
                playModel
                unstartedGameState
                viewResponse

        ReceivedPollForStartingInFailure errMsg ->
            PlayUpdate04PollForStartingIn.receivedPollForStartingInFailure
                playModel
                errMsg

        SendAddAnotherBotRequest startedGameState ->
            PlayUpdate03AddBot.sendAddAnotherBotRequest
                startedGameState
                playModel

        SendPollForRunning startedGameState ->
            PlayUpdate05PollForRunning.sendPollForRunning playModel
                startedGameState
