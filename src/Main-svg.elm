module Main exposing (main)

import Browser
import Html exposing (Html, div)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Time


main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    Int


type Msg
    = Tick Time.Posix


init : () -> ( Model, Cmd Msg )
init =
    \() -> ( 0, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick _ ->
            ( model + 1, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every 10 Tick


view : Model -> Html Msg
view model =
    div
        []
        [ svg
            [ width "200"
            , height "200"
            , viewBox "0 0 102 102"
            , fill "red"
            ]
            ([ rect [ x "1", y "1", width "101", height "101", fill "black" ] []
             , rect [ x "0", y "0", width "102", height "1", fill "grey" ] []
             , rect [ x "0", y "101", width "102", height "1", fill "grey" ] []
             , rect [ x "0", y "0", width "1", height "102", fill "grey" ] []
             , rect [ x "101", y "0", width "1", height "102", fill "grey" ] []
             ]
                ++ rects
                ++ [ text_
                        [ x "20", y "20", fill "white" ]
                        [ text (String.fromInt model) ]
                   ]
            )
        , text (String.fromInt model)
        ]


make_rect : Int -> Int -> Svg msg
make_rect x_coord y_coord =
    rect
        [ x (String.fromInt x_coord)
        , y (String.fromInt y_coord)
        , width "1.2"
        , height "1.2"
        ]
        []


rects : List (Svg msg)
rects =
    let
        ys : List Int
        ys =
            List.range 1 100

        xs : List Int
        xs =
            List.range 1 100
    in
    List.concatMap
        (\x -> List.map (make_rect x) ys)
        xs
