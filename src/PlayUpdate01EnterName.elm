module PlayUpdate01EnterName exposing (nameKeyPressed, sendJoinRequest)

import Constants
import Error2String exposing (error2String)
import Http
import JoinPost exposing (joinPost)
import JoinResponse exposing (JoinResponse)
import Json.Encode
import Msg exposing (Msg(..))
import PlayModel exposing (PlayModel, PlayState(..))
import PlayMsg exposing (PlayMsg(..))


nameKeyPressed : PlayModel -> String -> ( PlayModel, Cmd Msg )
nameKeyPressed playModel name =
    ( { playModel | name = name }, Cmd.none )


sendJoinRequest : PlayModel -> String -> ( PlayModel, Cmd Msg )
sendJoinRequest playModel name =
    let
        nonBlankName =
            name |> orMakeUpName
    in
    ( { playModel
        | name = nonBlankName
        , playState = WaitingToJoin
      }
    , joinPost nonBlankName processJoinResult
    )


processJoinResult : Result Http.Error JoinResponse -> Msg
processJoinResult result =
    case result of
        Ok res ->
            PlayMsg (ReceivedJoinSucceeded res)

        Err e ->
            PlayMsg <| ReceivedJoinFailed <| error2String e


orMakeUpName : String -> String
orMakeUpName name =
    if name == "" then
        "Tangler"

    else
        name
