module Error2String exposing (error2String)

import Http


error2String : Http.Error -> String
error2String error =
    case error of
        Http.BadUrl s ->
            "Bad url: " ++ s

        Http.Timeout ->
            "Timeout"

        Http.NetworkError ->
            "Network error"

        Http.BadStatus i ->
            "Bad status: " ++ String.fromInt i

        Http.BadBody s ->
            "Bad body: " ++ s
