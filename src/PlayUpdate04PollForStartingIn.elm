module PlayUpdate04PollForStartingIn exposing
    ( cancelPollingForStartingIn
    , receivedPollForStartingIn
    , receivedPollForStartingInFailure
    , sendPollForStartingIn
    )

import Constants
import Delay exposing (delay)
import Error2String exposing (error2String)
import GameStatus exposing (GameStatus(..))
import Http
import Msg exposing (Msg(..))
import PlayModel
    exposing
        ( PlayModel
        , PlayState(..)
        , StartedGameState
        , UnstartedGameState
        )
import PlayMsg exposing (PlayMsg(..))
import ViewResponse exposing (ViewResponse)
import ViewResponseParser


receivedPollForStartingIn :
    PlayModel
    -> UnstartedGameState
    -> ViewResponse
    -> ( PlayModel, Cmd Msg )
receivedPollForStartingIn playModel unstartedGameState viewResponse =
    case viewResponse.status of
        NotStarted ->
            let
                newState : UnstartedGameState
                newState =
                    unstartedGameState

                cmd : Cmd.Cmd Msg
                cmd =
                    delay 1000
                        -- TODO: more frequent?
                        (PlayMsg
                            (SendPollForStartingIn unstartedGameState)
                        )
            in
            ( { playModel | playState = WaitingForPlayers newState }, cmd )

        StartingIn t ->
            let
                newState : StartedGameState
                newState =
                    { gameid = viewResponse.gameid
                    , secret = unstartedGameState.secret
                    , index = unstartedGameState.index
                    , players = viewResponse.players
                    , t = viewResponse.t
                    , grid = ViewResponseParser.parseGrid viewResponse
                    }

                cmd : Cmd.Cmd Msg
                cmd =
                    delay
                        (toFloat
                            (if t < 1000 then
                                t

                             else
                                1000
                            )
                        )
                        (PlayMsg (SendPollForRunning newState))
            in
            ( { playModel | playState = WaitingForGame newState }, cmd )

        Running ->
            -- Shouldn't happen
            ( playModel, Cmd.none )

        Finished _ ->
            -- Shouldn't happen
            ( playModel, Cmd.none )


sendPollForStartingIn : PlayModel -> UnstartedGameState -> ( PlayModel, Cmd Msg )
sendPollForStartingIn playModel unstartedGameState =
    ( playModel
    , Http.get
        { url =
            Constants.server_url ++ "games/" ++ unstartedGameState.gameid
        , expect =
            Http.expectJson
                (processSendPollForStartingIn unstartedGameState)
                ViewResponse.decoder
        }
    )


receivedPollForStartingInFailure : PlayModel -> String -> ( PlayModel, Cmd Msg )
receivedPollForStartingInFailure playModel errMsg =
    let
        newPlayModel : PlayModel
        newPlayModel =
            { playModel | errors = errMsg :: playModel.errors }
    in
    ( newPlayModel, Cmd.none )


cancelPollingForStartingIn : PlayModel -> ( PlayModel, Cmd Msg )
cancelPollingForStartingIn playModel =
    ( { playModel | playState = AskingName }, Cmd.none )


processSendPollForStartingIn :
    UnstartedGameState
    -> Result Http.Error ViewResponse
    -> Msg
processSendPollForStartingIn unstartedGameState result =
    case result of
        Ok res ->
            PlayMsg <| ReceivedPollForStartingIn unstartedGameState res

        Err e ->
            PlayMsg <| ReceivedPollForStartingInFailure <| error2String e
