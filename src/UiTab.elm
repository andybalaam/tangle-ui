module UiTab exposing (UiTab(..), tabToString)


type UiTab
    = PlayTab
    | CodeTab
    | WatchTab


tabToString : UiTab -> String
tabToString uiTab =
    case uiTab of
        PlayTab ->
            "Play"

        CodeTab ->
            "Code"

        WatchTab ->
            "Watch"
