module GameStatus exposing (GameStatus(..), decoder)

import Json.Decode


type GameStatus
    = NotStarted
    | StartingIn Int
    | Running
    | Finished Int


decoder : Json.Decode.Decoder GameStatus
decoder =
    Json.Decode.oneOf
        [ Json.Decode.string
            |> Json.Decode.andThen
                (\s ->
                    case s of
                        "NotStarted" ->
                            Json.Decode.succeed NotStarted

                        "Running" ->
                            Json.Decode.succeed Running

                        _ ->
                            Json.Decode.fail
                                ("Unrecognised status: " ++ s)
                )
        , Json.Decode.field "StartingIn" Json.Decode.int
            |> Json.Decode.andThen (\i -> Json.Decode.succeed (StartingIn i))
        , Json.Decode.field "Finished" Json.Decode.int
            |> Json.Decode.andThen (\i -> Json.Decode.succeed (Finished i))
        ]
