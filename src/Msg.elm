module Msg exposing (Msg(..))

import Http
import PlayMsg exposing (PlayMsg)
import UiTab exposing (UiTab)


type Msg
    = ChooseTab UiTab
    | PlayMsg PlayMsg
