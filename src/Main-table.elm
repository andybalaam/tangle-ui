module Main exposing (main)

import Browser
import Html exposing (..)
import Html.Attributes exposing (..)
import Time


main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    Int


type Msg
    = Tick Time.Posix


init : () -> ( Model, Cmd Msg )
init =
    \() -> ( 0, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick _ ->
            ( model + 1, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every 1 Tick


view : Model -> Html Msg
view model =
    Html.div
        []
        [ grid model
        , text (String.fromInt model)
        ]


grid : Int -> Html msg
grid counter =
    table
        [ style "table-layout" "fixed"
        , style "border-collapse" "collapse"
        ]
        (List.map (row counter) (List.range 1 100))


row : Int -> Int -> Html msg
row counter y =
    tr
        []
        (List.map (cell counter y) (List.range 1 100))


cell : Int -> Int -> Int -> Html msg
cell counter y x =
    td
        [ style "background-color" (cell_colour counter y x)
        , style "width" "2px"
        , style "height" "2px"
        , style "border" "none"
        ]
        []


cell_colour : Int -> Int -> Int -> String
cell_colour counter y x =
    if (y - 1) * 100 + (x - 1) > counter then
        "black"

    else
        "red"
