module Delay exposing (delay)

import Process
import Task


delay : Float -> msg -> Cmd msg
delay timeMs msg =
    Process.sleep timeMs |> Task.perform (\_ -> msg)
