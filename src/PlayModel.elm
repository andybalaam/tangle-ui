module PlayModel exposing
    ( PlayModel
    , PlayState(..)
    , StartedGameState
    , UnstartedBot
    , UnstartedGameState
    , init
    )

import Player exposing (Player)


type alias PlayModel =
    { playState : PlayState
    , name : String
    , bots : List UnstartedBot
    , errors : List String
    }


type PlayState
    = AskingName
    | WaitingToJoin
    | WaitingForPlayers UnstartedGameState
    | WaitingToAddBot UnstartedGameState
    | WaitingForGame StartedGameState
    | WaitingToAddAnotherBot StartedGameState
    | FailedToJoin String


type alias UnstartedGameState =
    { gameid : String
    , secret : String
    , index : Int
    }


type alias StartedGameState =
    { gameid : String
    , secret : String
    , index : Int
    , players : List Player
    , t : Int
    , grid : List (List Int) -- Maybe these should be Arrays?
    }


type alias UnstartedBot =
    { name : String
    , secret : String
    }


init : PlayModel
init =
    { playState = AskingName
    , name = ""
    , bots = []
    , errors = []
    }
