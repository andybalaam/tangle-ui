module PlayViewCanvas exposing (gridToShapes, view)

import Canvas exposing (..)
import Canvas.Settings exposing (..)
import Color exposing (Color)
import Dict exposing (Dict)
import Html exposing (Html)
import Msg exposing (Msg)
import PlayModel exposing (PlayModel, StartedGameState)
import Player exposing (Player)


view : PlayModel -> StartedGameState -> Html Msg
view playState gameState =
    let
        width : Int
        width =
            300

        squareSize : Float
        squareSize =
            toFloat width / 100.0

        background : Renderable
        background =
            shapes
                [ fill Color.black ]
                [ rect ( 0, 0 ) (toFloat width) (toFloat width) ]

        player : Player
        player =
            nth
                gameState.index
                { name = "Unknown", pos = { x = 0, y = 0 } }
                gameState.players

        color : Color
        color =
            nth gameState.index Color.white allColors

        highlight : Renderable
        highlight =
            highlightPlayer squareSize color player.pos.x player.pos.y
    in
    Canvas.toHtml ( width, width )
        []
        ((background :: players 3.0 gameState.grid) ++ [ highlight ])


nth : Int -> a -> List a -> a
nth i default lst =
    lst |> List.drop i |> List.head |> Maybe.withDefault default


allColors : List Color.Color
allColors =
    [ Color.red
    , Color.green
    , Color.lightBlue
    , Color.lightPurple
    , Color.yellow
    , Color.orange
    ]


highlightPlayer : Float -> Color -> Int -> Int -> Renderable
highlightPlayer squareSize color x y =
    shapes
        [ stroke color ]
        [ circle
            ( toFloat x * squareSize + squareSize / 2.0, toFloat y * squareSize + squareSize / 2.0 )
            (5.0 * squareSize)
        ]


players : Float -> List (List Int) -> List Renderable
players squareSize grid =
    grid
        |> gridToShapes squareSize
        |> List.map2 (\color shps -> shapes [ fill color ] shps) allColors


gridToShapes : Float -> List (List Int) -> List (List Shape)
gridToShapes squareSize grid =
    grid
        |> List.indexedMap (row squareSize)
        |> List.concat
        |> List.foldl insertAppend Dict.empty
        |> toListByIndex []


toListByIndex : a -> Dict Int a -> List a
toListByIndex default dict =
    let
        maxI : Int
        maxI =
            dict |> Dict.keys |> List.maximum |> Maybe.withDefault 0

        getOrEmpty : Int -> a
        getOrEmpty i =
            dict |> Dict.get i |> Maybe.withDefault default
    in
    List.map getOrEmpty (List.range 1 maxI)


insertAppend : ( Int, Shape ) -> Dict Int (List Shape) -> Dict Int (List Shape)
insertAppend ( i, s ) dict =
    Dict.update i (newListOrAdd s) dict


newListOrAdd : a -> Maybe (List a) -> Maybe (List a)
newListOrAdd item maybeLst =
    case maybeLst of
        Nothing ->
            Just [ item ]

        Just lst ->
            Just (item :: lst)


row : Float -> Int -> List Int -> List ( Int, Shape )
row squareSize y nums =
    nums
        |> enumerate
        |> List.filterMap (square squareSize y)


enumerate : List a -> List ( Int, a )
enumerate ns =
    List.indexedMap (\i n -> ( i, n )) ns


square : Float -> Int -> ( Int, Int ) -> Maybe ( Int, Shape )
square squareSize y ( x, num ) =
    if num > 0 then
        Just
            ( num
            , rect
                ( toFloat x * squareSize
                , toFloat y * squareSize
                )
                squareSize
                squareSize
            )

    else
        Nothing
