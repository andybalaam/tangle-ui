module Player exposing (Player, decoder)

import Json.Decode


type alias Player =
    { name : String
    , pos : Position
    }


type alias Position =
    { x : Int
    , y : Int
    }


decoder : Json.Decode.Decoder Player
decoder =
    Json.Decode.map2
        Player
        (Json.Decode.field "name" Json.Decode.string)
        (Json.Decode.field "pos" positionDecoder)


positionDecoder : Json.Decode.Decoder Position
positionDecoder =
    Json.Decode.map2
        Position
        (Json.Decode.field "x" Json.Decode.int)
        (Json.Decode.field "y" Json.Decode.int)
