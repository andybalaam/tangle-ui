module ViewResponse exposing (ViewResponse, decoder)

import GameStatus exposing (GameStatus)
import Json.Decode
import Player exposing (Player)


type alias ViewResponse =
    { gameid : String
    , status : GameStatus
    , t : Int
    , players : List Player
    , grid : List String
    }


decoder : Json.Decode.Decoder ViewResponse
decoder =
    Json.Decode.map5
        ViewResponse
        (Json.Decode.field "gameid" Json.Decode.string)
        (Json.Decode.field "status" GameStatus.decoder)
        (Json.Decode.field "t" Json.Decode.int)
        (Json.Decode.field "players" (Json.Decode.list Player.decoder))
        (Json.Decode.field "grid" (Json.Decode.list Json.Decode.string))
