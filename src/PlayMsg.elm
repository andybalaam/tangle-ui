module PlayMsg exposing (PlayMsg(..))

import JoinResponse exposing (JoinResponse)
import PlayModel exposing (StartedGameState, UnstartedGameState)
import ViewResponse exposing (ViewResponse)


type PlayMsg
    = NameKeyPressed String
    | SendJoinRequest String
    | CancelJoinRequest
    | ReceivedJoinFailed String
    | ReceivedJoinSucceeded JoinResponse
    | SendAddBotRequest UnstartedGameState
    | SendAddAnotherBotRequest StartedGameState
    | ReceivedAddBotFailed String UnstartedGameState
    | ReceivedAddBotSucceeded UnstartedGameState JoinResponse
    | ReceivedAddAnotherBotFailed String StartedGameState
    | ReceivedAddAnotherBotSucceeded StartedGameState JoinResponse
    | CancelPollingForStartingIn
    | SendPollForStartingIn UnstartedGameState
    | ReceivedPollForStartingIn UnstartedGameState ViewResponse
    | ReceivedPollForStartingInFailure String
    | SendPollForRunning StartedGameState
