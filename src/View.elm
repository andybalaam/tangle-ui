module View exposing (view)

-- import Canvas exposing (..)
-- import Canvas.Settings exposing (..)

import Browser exposing (Document)
import Color
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Model exposing (Model)
import Msg exposing (Msg(..))
import PlayView
import UiTab exposing (UiTab(..), tabToString)


view : Model -> Document Msg
view model =
    { title = "Tangle"
    , body =
        [ Element.layout [ Background.color pageBackgroundColor, padding 10 ]
            (column [ height fill, width fill, spacing 0 ]
                [ tabBar model
                , tabContents model
                ]
            )
        ]
    }


pageBackgroundColor : Color
pageBackgroundColor =
    rgb255 64 68 81


tabBar : Model -> Element Msg
tabBar model =
    row [ Region.navigation, width fill, spacing 5 ]
        [ tab PlayTab model, tab CodeTab model, tab WatchTab model ]


selectedTabStyle : List (Attribute Msg)
selectedTabStyle =
    [ Background.color <| rgb255 33 35 41
    , Font.color <| rgb255 227 277 277
    ]


unselectedTabStyle : List (Attribute Msg)
unselectedTabStyle =
    [ Background.color <| rgb255 52 54 77
    , Font.color <| rgb255 120 120 150
    ]


tab : UiTab -> Model -> Element Msg
tab thisTab model =
    let
        details =
            if thisTab == model.uiTab then
                selectedTabStyle

            else
                unselectedTabStyle
    in
    Input.button
        ([ padding 16
         , width (shrink |> minimum 100)
         , Border.roundEach
            { topLeft = 10, topRight = 10, bottomLeft = 0, bottomRight = 0 }
         ]
            ++ details
        )
        { onPress = Just (ChooseTab thisTab)
        , label = el [ centerX ] (text (tabToString thisTab))
        }


tabContents : Model -> Element Msg
tabContents model =
    el
        ([ width fill
         , height fill
         ]
            ++ selectedTabStyle
        )
        (el
            [ padding 10, centerX, centerY ]
            (case model.uiTab of
                PlayTab ->
                    PlayView.view model.playModel

                CodeTab ->
                    code model

                WatchTab ->
                    watch model
            )
        )


code : Model -> Element Msg
code model =
    text "Code"


watch : Model -> Element Msg
watch model =
    text "Watch"
