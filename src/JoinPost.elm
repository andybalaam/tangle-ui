module JoinPost exposing (joinPost)

import Constants
import Http
import JoinResponse exposing (JoinResponse)
import Json.Encode
import Msg exposing (Msg)


joinPost : String -> (Result Http.Error JoinResponse -> Msg) -> Cmd Msg
joinPost botName resultProcessor =
    Http.post
        { url = Constants.server_url ++ "join"
        , body = Http.jsonBody (joinRequest botName)
        , expect = Http.expectJson resultProcessor JoinResponse.decoder
        }


joinRequest : String -> Json.Encode.Value
joinRequest name =
    Json.Encode.object [ ( "name", Json.Encode.string name ) ]
