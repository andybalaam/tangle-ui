module Main exposing (main)

import Browser exposing (Document)
import Flags exposing (Flags)
import Model exposing (Model)
import Msg exposing (Msg(..))
import PlayModel exposing (PlayModel, PlayState(..))
import PlayUpdate
import View


main =
    Browser.document
        { init = init
        , view = View.view
        , update = update
        , subscriptions = subscriptions
        }


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( Model.init, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ChooseTab uiTab ->
            ( { model | uiTab = uiTab }, Cmd.none )

        PlayMsg playMsg ->
            let
                ( newPlayModel, cmd ) =
                    PlayUpdate.update playMsg model.playModel
            in
            ( { model | playModel = newPlayModel }, cmd )
