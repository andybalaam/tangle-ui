module PlayUpdate02Join exposing
    ( cancelJoinRequest
    , receivedJoinFailed
    , receivedJoinSucceeded
    )

import Delay exposing (delay)
import JoinResponse exposing (JoinResponse)
import Msg exposing (Msg(..))
import PlayModel exposing (PlayModel, PlayState(..), UnstartedGameState)
import PlayMsg exposing (PlayMsg(..))


cancelJoinRequest : PlayModel -> ( PlayModel, Cmd Msg )
cancelJoinRequest playModel =
    ( { playModel | playState = AskingName }, Cmd.none )


receivedJoinSucceeded :
    PlayModel
    -> JoinResponse
    -> ( PlayModel, Cmd Msg )
receivedJoinSucceeded playModel joinResponse =
    let
        unstartedGameState : UnstartedGameState
        unstartedGameState =
            { gameid = joinResponse.gameid
            , secret = joinResponse.secret
            , index = joinResponse.index
            }

        newPlayModel : PlayModel
        newPlayModel =
            { playModel
                | playState = WaitingForPlayers unstartedGameState
                , name = joinResponse.name
            }
    in
    ( newPlayModel
      -- TODO: more frequent?
    , delay 1000 (PlayMsg (SendPollForStartingIn unstartedGameState))
    )


receivedJoinFailed : PlayModel -> String -> ( PlayModel, Cmd Msg )
receivedJoinFailed playModel errMsg =
    let
        newPlayModel : PlayModel
        newPlayModel =
            { playModel
                | playState = FailedToJoin errMsg
            }
    in
    ( newPlayModel, Cmd.none )
