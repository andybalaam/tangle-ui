module JoinResponse exposing (JoinResponse, decoder)

import Json.Decode


type alias JoinResponse =
    { gameid : String
    , name : String
    , index : Int
    , secret : String
    }


decoder : Json.Decode.Decoder JoinResponse
decoder =
    Json.Decode.map4
        JoinResponse
        (Json.Decode.field "gameid" Json.Decode.string)
        (Json.Decode.field "name" Json.Decode.string)
        (Json.Decode.field "index" Json.Decode.int)
        (Json.Decode.field "secret" Json.Decode.string)
