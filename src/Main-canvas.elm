module Main exposing (main)

import Browser
import Canvas exposing (..)
import Canvas.Settings exposing (..)
import Color
import Html exposing (Html, div, text)
import Html.Attributes exposing (style)
import Time


main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


type alias Model =
    Int


type Msg
    = Tick Time.Posix


init : () -> ( Model, Cmd Msg )
init =
    \() -> ( 0, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick _ ->
            ( model + 1, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Time.every 10 Tick


view : Model -> Html Msg
view model =
    div
        []
        [ make_canvas
        , text (String.fromInt model)
        ]


make_canvas : Html Msg
make_canvas =
    let
        width =
            200

        height =
            200
    in
    Canvas.toHtml ( width, height )
        []
        [ shapes [ fill Color.black ] [ rect ( 0, 0 ) width height ]
        , rects
        ]


rects : Renderable
rects =
    shapes [ fill Color.red ]
        (List.concatMap
            (\x -> List.concatMap (r x) (List.range 0 100))
            (List.range 0 100)
        )


r : Int -> Int -> List Shape
r x y =
    if (x + y * 100 |> remainderBy 3) == 0 then
        [ rect ( toFloat x * 2, toFloat y * 2 ) 2 2 ]

    else
        []
