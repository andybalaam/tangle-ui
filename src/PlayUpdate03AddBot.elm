module PlayUpdate03AddBot exposing
    ( receivedAddAnotherBotFailed
    , receivedAddAnotherBotSucceeded
    , receivedAddBotFailed
    , receivedAddBotSucceeded
    , sendAddAnotherBotRequest
    , sendAddBotRequest
    )

import Constants
import Delay exposing (delay)
import Error2String exposing (error2String)
import Http
import JoinPost exposing (joinPost)
import JoinResponse exposing (JoinResponse)
import Msg exposing (Msg(..))
import PlayModel
    exposing
        ( PlayModel
        , PlayState(..)
        , StartedGameState
        , UnstartedBot
        , UnstartedGameState
        )
import PlayMsg exposing (PlayMsg(..))


sendAddBotRequest : UnstartedGameState -> PlayModel -> ( PlayModel, Cmd Msg )
sendAddBotRequest unstartedGameState playModel =
    ( { playModel | playState = WaitingToAddBot unstartedGameState }
    , joinPost "abot" (processBotJoinResult unstartedGameState)
      -- TODO: unique name
    )


sendAddAnotherBotRequest :
    StartedGameState
    -> PlayModel
    -> ( PlayModel, Cmd Msg )
sendAddAnotherBotRequest startedGameState playModel =
    ( { playModel | playState = WaitingToAddAnotherBot startedGameState }
    , joinPost "abot2" (processAnotherBotJoinResult startedGameState)
      -- TODO: unique name
    )


processBotJoinResult :
    UnstartedGameState
    -> Result Http.Error JoinResponse
    -> Msg
processBotJoinResult unstartedGameState result =
    case result of
        Ok res ->
            PlayMsg (ReceivedAddBotSucceeded unstartedGameState res)

        Err e ->
            PlayMsg <|
                ReceivedAddBotFailed (error2String e) unstartedGameState


processAnotherBotJoinResult :
    StartedGameState
    -> Result Http.Error JoinResponse
    -> Msg
processAnotherBotJoinResult startedGameState result =
    case result of
        Ok res ->
            PlayMsg (ReceivedAddAnotherBotSucceeded startedGameState res)

        Err e ->
            PlayMsg <|
                ReceivedAddAnotherBotFailed (error2String e) startedGameState


receivedAddBotSucceeded :
    UnstartedGameState
    -> PlayModel
    -> JoinResponse
    -> ( PlayModel, Cmd Msg )
receivedAddBotSucceeded unstartedGameState playModel joinResponse =
    let
        newPlayModel : PlayModel
        newPlayModel =
            if joinResponse.gameid == unstartedGameState.gameid then
                { playModel
                    | playState = WaitingForPlayers unstartedGameState
                    , bots = addBot playModel joinResponse
                }

            else
                { playModel
                    | playState = WaitingForPlayers unstartedGameState
                    , errors = "No more bots can be added." :: playModel.errors
                }
    in
    ( newPlayModel
    , delay 1 (PlayMsg (SendPollForStartingIn unstartedGameState))
    )


receivedAddAnotherBotSucceeded :
    StartedGameState
    -> PlayModel
    -> JoinResponse
    -> ( PlayModel, Cmd Msg )
receivedAddAnotherBotSucceeded startedGameState playModel joinResponse =
    let
        newPlayModel : PlayModel
        newPlayModel =
            if joinResponse.gameid == startedGameState.gameid then
                { playModel
                    | playState = WaitingForGame startedGameState
                    , bots = addBot playModel joinResponse
                }

            else
                { playModel
                    | playState = WaitingForGame startedGameState
                    , errors = "No more bots can be added." :: playModel.errors
                }
    in
    ( newPlayModel
    , delay 1 (PlayMsg (SendPollForRunning startedGameState))
    )


addBot : PlayModel -> JoinResponse -> List UnstartedBot
addBot playModel joinResponse =
    { name = joinResponse.name
    , secret = joinResponse.secret
    }
        :: playModel.bots


receivedAddBotFailed :
    PlayModel
    -> String
    -> UnstartedGameState
    -> ( PlayModel, Cmd Msg )
receivedAddBotFailed playModel errMsg unstartedGameState =
    let
        newPlayModel : PlayModel
        newPlayModel =
            { playModel
                | playState = WaitingForPlayers unstartedGameState
                , errors = errMsg :: playModel.errors
            }
    in
    ( newPlayModel
    , delay 1000 (PlayMsg (SendPollForStartingIn unstartedGameState))
    )


receivedAddAnotherBotFailed :
    PlayModel
    -> String
    -> StartedGameState
    -> ( PlayModel, Cmd Msg )
receivedAddAnotherBotFailed playModel errMsg startedGameState =
    let
        newPlayModel : PlayModel
        newPlayModel =
            { playModel
                | playState = WaitingForGame startedGameState
                , errors = errMsg :: playModel.errors
            }
    in
    ( newPlayModel
    , delay 1000 (PlayMsg (SendPollForRunning startedGameState))
    )
