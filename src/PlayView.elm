module PlayView exposing (view)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Msg exposing (Msg(..))
import OnEnter exposing (onEnter)
import PlayModel
    exposing
        ( PlayModel
        , PlayState(..)
        , StartedGameState
        , UnstartedGameState
        )
import PlayMsg exposing (PlayMsg(..))
import PlayViewCanvas


view : PlayModel -> Element Msg
view playModel =
    case playModel.playState of
        AskingName ->
            askingName playModel.name

        WaitingToJoin ->
            waitingToJoin playModel

        WaitingForPlayers unstartedGameState ->
            waitingForPlayers playModel unstartedGameState

        WaitingForGame startedGameState ->
            waitingForGame playModel startedGameState

        WaitingToAddBot unstartedGameState ->
            waitingToSendAddBotRequest playModel unstartedGameState

        WaitingToAddAnotherBot startedGameState ->
            waitingToSendAddAnotherBotRequest playModel startedGameState

        FailedToJoin errMsg ->
            failedToJoin errMsg playModel


failedToJoin : String -> PlayModel -> Element Msg
failedToJoin errMsg playModel =
    column
        formStyle
        [ text "Failed to join a game!"
        , text <| "Error: " ++ errMsg
        , Input.button
            ([ width fill ] ++ buttonStyle)
            { onPress = Just (PlayMsg (SendJoinRequest playModel.name))
            , label = el [ centerX, padding 10 ] (text "Try again")
            }
        ]


errors : List String -> Element Msg
errors errorList =
    column
        formStyle
        (List.map text errorList)


waitingToJoin : PlayModel -> Element Msg
waitingToJoin playModel =
    column
        formStyle
        [ text "Joining a game..."
        , text <| "Name: " ++ playModel.name
        , errors playModel.errors
        , Input.button
            ([ width fill ] ++ buttonStyle)
            { onPress = Just (PlayMsg CancelJoinRequest)
            , label = el [ centerX, padding 10 ] (text "Cancel")
            }
        ]


waitingForPlayers : PlayModel -> UnstartedGameState -> Element Msg
waitingForPlayers playModel unstartedGameState =
    column
        formStyle
        [ text "Waiting for other players..."
        , text <| "Name: " ++ playModel.name
        , errors playModel.errors
        , Input.button
            ([ width fill ] ++ buttonStyle)
            { onPress = Just (PlayMsg CancelJoinRequest)
            , label = el [ centerX, padding 10 ] (text "Cancel")
            }
        , Input.button
            ([ width fill ] ++ buttonStyle)
            { onPress = Just (PlayMsg (SendAddBotRequest unstartedGameState))
            , label = el [ centerX, padding 10 ] (text "Add a bot")
            }
        ]


waitingForGame : PlayModel -> StartedGameState -> Element Msg
waitingForGame playModel startedGameState =
    runningGame playModel
        startedGameState
        (botsMessage playModel)
        "Waiting for game to start..."


waitingToSendAddAnotherBotRequest :
    PlayModel
    -> StartedGameState
    -> Element Msg
waitingToSendAddAnotherBotRequest playModel startedGameState =
    runningGame playModel
        startedGameState
        (botsMessage playModel)
        "Waiting for game to start (adding bot)..."


botsMessage : PlayModel -> List (Element Msg)
botsMessage playModel =
    let
        numBots : Int
        numBots =
            List.length playModel.bots
    in
    if numBots == 0 then
        []

    else
        [ text <| "Added bots: " ++ String.fromInt numBots ]


runningGame :
    PlayModel
    -> StartedGameState
    -> List (Element Msg)
    -> String
    -> Element Msg
runningGame playModel startedGameState botsMsg msg =
    column
        formStyle
        ([ text msg
         , text <| "Name: " ++ playModel.name
         , errors playModel.errors
         , html <| PlayViewCanvas.view playModel startedGameState
         ]
            ++ botsMsg
            ++ [ row
                    (formStyle ++ [ width fill ])
                    [ Input.button
                        ([ width fill ] ++ buttonStyle)
                        { onPress = Just (PlayMsg CancelPollingForStartingIn)
                        , label = el [ centerX, padding 10 ] (text "Quit game")
                        }
                    , Input.button
                        ([ width fill ] ++ buttonStyle)
                        { onPress =
                            Just
                                (PlayMsg
                                    (SendAddAnotherBotRequest
                                        startedGameState
                                    )
                                )
                        , label = el [ centerX, padding 10 ] (text "Add a bot")
                        }
                    ]
               ]
        )


waitingToSendAddBotRequest : PlayModel -> UnstartedGameState -> Element Msg
waitingToSendAddBotRequest playModel unstartedGameState =
    column
        formStyle
        [ text "Adding a bot..."
        , text <| "Name: " ++ playModel.name
        , errors playModel.errors
        , Input.button
            ([ width fill ] ++ buttonStyle)
            { onPress = Just (PlayMsg CancelPollingForStartingIn)
            , label = el [ centerX, padding 10 ] (text "Cancel game")
            }
        ]


askingName : String -> Element Msg
askingName name =
    column
        formStyle
        [ Input.text
            ([ onEnter (PlayMsg (SendJoinRequest name)) ] ++ textInputStyle)
            { onChange = \n -> PlayMsg (NameKeyPressed n)
            , text = name
            , placeholder = Just <| Input.placeholder [] (text "E.g. Susan")
            , label = Input.labelAbove formLabelStyle (text "Enter your name:")
            }
        , Input.button
            ([ width fill ] ++ buttonStyle)
            { onPress = Just (PlayMsg (SendJoinRequest name))
            , label = el [ centerX, padding 10 ] (text "Join a game!")
            }
        ]


buttonStyle : List (Attribute Msg)
buttonStyle =
    [ Background.color <| rgb255 100 100 200
    , Font.color <| rgb255 200 200 255
    , Border.rounded 10
    ]


textInputStyle : List (Attribute Msg)
textInputStyle =
    [ Background.color <| rgb255 255 255 255
    , Font.color <| rgb255 0 0 0
    ]


formStyle : List (Attribute Msg)
formStyle =
    [ Font.size 25, spacing 15 ]


formLabelStyle : List (Attribute Msg)
formLabelStyle =
    [ centerX, padding 10 ]
