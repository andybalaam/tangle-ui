module PlayUpdate05PollForRunning exposing (sendPollForRunning)

import Msg exposing (Msg(..))
import PlayModel exposing (PlayModel, StartedGameState)


sendPollForRunning : PlayModel -> StartedGameState -> ( PlayModel, Cmd Msg )
sendPollForRunning playModel startedGameState =
    ( playModel, Cmd.none )



-- TODO
