module Model exposing (Model, init)

import PlayModel exposing (PlayModel)
import UiTab exposing (UiTab)


type alias Model =
    { uiTab : UiTab
    , playModel : PlayModel
    }


init : Model
init =
    { uiTab = UiTab.PlayTab
    , playModel = PlayModel.init
    }
