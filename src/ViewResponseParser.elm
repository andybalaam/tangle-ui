module ViewResponseParser exposing (parseGrid)

import ViewResponse exposing (ViewResponse)


parseGrid : ViewResponse -> List (List Int)
parseGrid viewResponse =
    viewResponse.grid |> List.map parseRow


parseRow : String -> List Int
parseRow row =
    String.toList row |> List.map parseChar


parseChar : Char -> Int
parseChar c =
    let
        code =
            Char.toCode c
    in
    if code >= 65 && code <= 90 then
        code - 64

    else if code >= 97 && code <= 122 then
        code - 96

    else
        0
