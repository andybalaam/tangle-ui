module TestPlayViewCanvas exposing (..)

import Canvas exposing (Shape, rect)
import Dict
import Expect exposing (Expectation)
import PlayViewCanvas exposing (gridToShapes)
import Test exposing (..)


suite : Test
suite =
    describe "Shapes from grids"
        [ test "One colour, 2px square" <|
            ([ [ 0, 1, 0 ], [ 1, 0, 0 ], [ 0, 0, 0 ] ]
                |> rendersTo 2
                    [ [ rect ( 0, 2 ) 2 2, rect ( 2, 0 ) 2 2 ] ]
            )
        , test "One colour, fractional square" <|
            ([ [ 0, 1, 0 ], [ 0, 0, 1 ], [ 0, 0, 0 ] ]
                |> rendersTo 3.5
                    [ [ rect ( 7.0, 3.5 ) 3.5 3.5, rect ( 3.5, 0 ) 3.5 3.5 ] ]
            )
        , test "Two colours" <|
            ([ [ 0, 1, 0 ], [ 2, 0, 0 ], [ 0, 0, 0 ] ]
                |> rendersTo 1
                    [ [ rect ( 1, 0 ) 1 1 ]
                    , [ rect ( 0, 1 ) 1 1 ]
                    ]
            )
        , test "Two colours out of three" <|
            ([ [ 0, 1, 0 ], [ 3, 0, 0 ], [ 0, 0, 0 ] ]
                |> rendersTo 1
                    [ [ rect ( 1, 0 ) 1 1 ]
                    , []
                    , [ rect ( 0, 1 ) 1 1 ]
                    ]
            )
        ]


rendersTo :
    Float
    -> List (List Shape)
    -> List (List Int)
    -> (() -> Expectation)
rendersTo squareSize expected grid =
    \_ ->
        Expect.equal (gridToShapes squareSize grid) expected
