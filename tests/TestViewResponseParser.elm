module TestViewResponseParser exposing (..)

import Expect exposing (Expectation)
import GameStatus exposing (GameStatus(..))
import Test exposing (..)
import ViewResponse exposing (ViewResponse)
import ViewResponseParser


suite : Test
suite =
    describe "Parsing view responses"
        [ test "String grid" <|
            (viewResponse [ ".a.", "B..", "..." ]
                |> parsesTo
                    [ [ 0, 1, 0 ], [ 2, 0, 0 ], [ 0, 0, 0 ] ]
            )
        ]


viewResponse : List String -> ViewResponse
viewResponse grid =
    { gameid = "", status = Running, players = [], t = 0, grid = grid }


parsesTo : List (List Int) -> ViewResponse -> (() -> Expectation)
parsesTo expected response =
    \_ ->
        Expect.equal
            (ViewResponseParser.parseGrid response)
            expected
