module TestDecoders exposing (..)

import Expect exposing (Expectation)
import GameStatus exposing (GameStatus(..))
import JoinResponse
import Json.Decode
import Test exposing (..)
import ViewResponse


suite : Test
suite =
    describe "JSON decoding"
        [ test "/join" <|
            decodesOk
                JoinResponse.decoder
                """{"gameid":"g","name":"n","index":1,"secret":"s"}"""
                { gameid = "g", name = "n", index = 1, secret = "s" }
        , test "GameStatus NotStarted" <|
            decodesOk
                GameStatus.decoder
                "\"NotStarted\""
                NotStarted
        , test "GameStatus StartingIn" <|
            decodesOk
                GameStatus.decoder
                """{"StartingIn": 4000}"""
                (StartingIn 4000)
        , test "GameStatus Running" <|
            decodesOk
                GameStatus.decoder
                "\"Running\""
                Running
        , test "GameStatus Finished" <|
            decodesOk
                GameStatus.decoder
                """{"Finished": 3000}"""
                (Finished 3000)
        , test "/games/{id}" <|
            decodesOk
                ViewResponse.decoder
                """{"gameid":"gi"
                ,   "status":"Running"
                ,   "players": [
                        {"name": "A", "pos": {"x": 3, "y": 5}},
                        {"name": "B", "pos": {"x": 1, "y": 2}}
                    ]
                ,   "t":13
                ,   "grid":["a..b","A..B"]}"""
                { gameid = "gi"
                , status = Running
                , players =
                    [ { name = "A", pos = { x = 3, y = 5 } }
                    , { name = "B", pos = { x = 1, y = 2 } }
                    ]
                , t = 13
                , grid = [ "a..b", "A..B" ]
                }
        ]


decodesOk decoder string expected =
    \_ ->
        Expect.equal
            (Json.Decode.decodeString decoder string)
            (Ok expected)
